import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/Login")
selenium.type("id=UserName", GlobalVariable.endUserName)
selenium.click("name=submit")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Return to login")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.type("id=Password", GlobalVariable.endPassword)
selenium.click("name=submit")
selenium.waitForPageToLoad("30000")

//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=ChallengeQuestion")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//selenium.type("id=ChallengeQuestion", "test")
//selenium.click("//button[@data-stable-name='ContinueButton']")


for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[contains(text(),'Messages and Alerts')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[contains(text(),'Messages and Alerts')]")
selenium.click("//div[contains(text(),'Inbox')]")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//span[text()='Sort by date']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Messages and Alerts Inbox End User.png")

selenium.click("//div[contains(text(),'Manage Alerts')]")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//span[text()='Secure Message Alert'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(5000);
selenium.click("xpath=(//span[text()='Secure Message Alert'])[2]")
Thread.sleep(1000);
selenium.click("xpath=(//span[text()='Change in Contact Info Alert'])[2]")
Thread.sleep(1000);
selenium.click("xpath=(//span[text()='Login Credential Change Alert'])[2]")
Thread.sleep(1000);
selenium.click("xpath=(//span[text()='Login Alert'])[2]")
Thread.sleep(1000);
selenium.click("xpath=(//span[text()='Password Reset Request Alert'])[2]")

Thread.sleep(6000);

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Messages and Alerts Manage Alerts General End User.png")

selenium.click("xpath=(//span[contains(text(),'2013 HONDA CR-V')])[1]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//span[@class='toggle-header__message']//span[contains(text(),'Balance Summary Alert')])")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(1000);
selenium.click("xpath=(//span[contains(text(), 'Balance Summary Alert')])[2]")
Thread.sleep(1000);
selenium.click("xpath=(//span[contains(text()='Balance Alert')])")
Thread.sleep(1000);
selenium.click("xpath=(//span[contains(text()='Transaction Size Alert')])")
Thread.sleep(1000);
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@class='btn btn--primary'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Messages and Alerts Manage Alerts CAR FUND End User.png")

selenium.click("xpath=(//span[text()='0040 CHECKING - XX7232 - S:0040'])[1]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//span[text()='Balance Summary'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("xpath=(//span[text()='Balance Summary Alert'])[1]")
selenium.click("xpath=(//span[text()='Balance Alert'])[1]")
selenium.click("xpath=(//span[text()='Transaction Size Alert'])[1]")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@class='btn btn--primary'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Messages and Alerts Manage Alerts PRIME SHARE SAVINGS End User.png")

selenium.click("link=Sign out")
selenium.waitForPageToLoad("30000")