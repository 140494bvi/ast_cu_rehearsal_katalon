import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/Login")
selenium.type("id=UserName", GlobalVariable.endUserName)
selenium.click("name=submit")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Return to login")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.type("id=Password", GlobalVariable.endPassword)
selenium.click("name=submit")
selenium.waitForPageToLoad("30000")

//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=ChallengeQuestion")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//selenium.type("id=ChallengeQuestion", "test")
//selenium.click("//button[@data-stable-name='ContinueButton']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//a[@title='Settings']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//a[@title='Settings']")
selenium.click("//div[contains(text(),'Login Settings')]")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//span[text()='Username'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("xpath=(//span[text()='Username'])[1]")
selenium.click("xpath=(//span[text()='Password'])[1]")
selenium.click("xpath=(//span[text()='Challenge questions'])[1]")
selenium.click("xpath=(//span[text()='Security phrase'])[1]")
selenium.click("xpath=(//span[text()='Security image'])[1]")
selenium.click("xpath=(//span[text()='Automatic log-off'])[1]")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@data-stable-name='SaveButton'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Settings Login Settings End User.png")

//selenium.click("//div[text()='Contact Information']")
//
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("xpath=(//h4[contains(text(),'Email')])")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Settings Contact Information End User.png")

selenium.click("//div[text()='Additional Services']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//span[contains(text(),'Time Zone')])")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("xpath=(//span[contains(text(),'Time Zone')])")
selenium.click("xpath=(//span[text()='Challenge Question Each Login'])[1]")
selenium.click("xpath=(//span[text()='E-Statements'])[1]")
selenium.click("xpath=(//span[text()='One-time PIN authentication'])[1]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@data-stable-name='SaveButton'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(4000);
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Settings Additional Services End User.png")

selenium.click("//div[text()='Event Logs']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//table[@role='grid'])[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(3000);
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Settings Event Logs End User.png")


selenium.click("link=Sign out")
selenium.waitForPageToLoad("30000")