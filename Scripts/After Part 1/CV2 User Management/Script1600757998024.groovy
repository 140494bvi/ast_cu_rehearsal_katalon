import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/admin")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//input[@value='Sign in']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(1000);
selenium.type("id=username", GlobalVariable.adminUserName)
selenium.type("id=password", GlobalVariable.adminPassword)
selenium.click("//input[@value='Sign in']")
selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=User Management")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("link=User Management")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//input[@value='Clear Sort/Filter']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

Thread.sleep(5000);

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - User magagement (disabled user)after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - User magagement.png")
//
//selenium.type("id=UserId", "cvumtest1")
//selenium.click("name=Search")
//selenium.waitForPageToLoad("30000")
//
//Thread.sleep(3000)
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - User magagement (disabled user)after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - User magagement (disabled user)after upgrade.png")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=btnBackToUserManagement")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//selenium.click("id=btnBackToUserManagement")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//input[@value='Clear Sort/Filter']")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
//selenium.type("id=UserId", "cvumtest2")
//selenium.click("name=Search")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//input[@type='checkbox']")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
//selenium.click("//input[@type='checkbox']")
//Thread.sleep(2000)
////selenium.click("name=DetailView")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//div[@id='dUserDetailTabs-1']/dl/dt[3]")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - User magagement ( reset password user)after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - User magagement ( reset password user)after upgrade.png")
//selenium.click("id=btnBackToUserManagement")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//input[@value='Clear Sort/Filter']")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
//selenium.type("id=UserId", "cvumtest")
//selenium.click("name=Search")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//input[@type='checkbox']")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
//selenium.click("//input[@type='checkbox']")
////selenium.click("name=DetailView")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//div[@id='dUserDetailTabs-1']/dl/dt[3]")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - user magagement (locked user)after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - user magagement (locked user)after upgrade.png")
selenium.click("link=Logout")
