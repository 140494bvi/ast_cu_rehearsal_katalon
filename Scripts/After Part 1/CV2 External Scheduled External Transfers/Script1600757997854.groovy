import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/Login")
selenium.type("id=UserName", "gremlyn202")
selenium.click("name=submit")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Return to login")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.type("id=Password", "Test123!")
selenium.click("name=submit")
selenium.waitForPageToLoad("30000")
//
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=ChallengeQuestion")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//selenium.type("id=ChallengeQuestion", "test")
//selenium.click("//button[@data-stable-name='ContinueButton']")
//

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//a[@title='Transfers & Payments']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//a[@title='Transfers & Payments']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[contains(text(), 'Scheduled Transfers')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
selenium.click("//div[contains(text(), 'Scheduled Transfers')]")
Thread.sleep(3000)
//selenium.click("//td[contains(text(), 'Once')]")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Scheduled Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Scheduled Transfer.png")
//selenium.click("xpath=(//*[@id='wrapper__workarea']/section/div[2]/div/div/div/div[3]/div[2]/table/tbody/tr[1]/td[1]/a)")
//selenium.click("//td[contains(text(), 'Daily')]")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("xpath=(//button[@type='button'])[10]")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Repeating Transfer.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Repeating Transfer.png")
selenium.click("//div[contains(text(), 'Make a Transfer')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//*[contains(text(), '1. Where is the money coming from?')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(3000);
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - External Transfer.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Make a Transfer.png")
selenium.click("link=Sign out")
selenium.waitForPageToLoad("30000")
