import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium

import internal.GlobalVariable

import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/login")

selenium.type("id=UserName", GlobalVariable.endUserName)
selenium.click("name=submit")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Return to login")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.type("id=Password", GlobalVariable.endPassword)
selenium.click("name=submit")
selenium.waitForPageToLoad("30000")

//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=ChallengeQuestion")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//selenium.type("id=ChallengeQuestion", "test")
//selenium.click("//button[@data-stable-name='ContinueButton']")

//selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Authorized User")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("link=Authorized User")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("//span[text()='PRIME SHARE SAVINGS  (21594 - S:00)']")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}

//selenium.click("//span[text()='PRIME SHARE SAVINGS  (21594 - S:00)']")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("xpath=html/body/div[2]/div/div/div/section/div[2]/div/div/section[1]/div/div/div/div[4]/div[1]/div/div[1]/div[2]/div/table/thead/tr/th[1]")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
//selenium.click("xpath=html/body/div[2]/div/div/div/section/div[2]/div/div/section[1]/div/div/div/div[4]/div[1]/div/div[2]/div[1]/span/span[1]")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("xpath=html/body/div[2]/div/div/div/section/div[2]/div/div/section[1]/div/div/div/div[4]/div[1]/div/div[2]/div[2]/div/table/thead/tr/th[1]")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}

Thread.sleep(5000);

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Family & Friends(EndUser) after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Family & Friends(EndUser) after.png")
selenium.click("link=Sign out")
selenium.waitForPageToLoad("30000")
