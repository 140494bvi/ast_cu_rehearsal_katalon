import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.junit.After
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/admin")
selenium.type("id=username", GlobalVariable.adminUserName)
selenium.type("id=password", GlobalVariable.adminPassword)
selenium.click("//input[@value='Sign in']")
Thread.sleep(1000);
for (int second = 0;; second++) {
	if (second >= 70) fail("timeout");
	try { if (selenium.isVisible("link=Introduction to Reports")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(1000);
selenium.click("link=Introduction to Reports")
selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='content']/div/h1")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(3000);
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Introduction to Reports ) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (Introduction to Reports ) after upgrade.png")
//selenium.click("xpath=(//a[contains(text(),'ACH Transfer Velocity')])[2]")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports ( ACH Transfer Velocity) after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports ( ACH Transfer Velocity) after upgrade.png")
//selenium.click("link=Active Online Banking User Accounts")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Active Online Banking User Accounts) after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (Active Online Banking User Accounts) after upgrade.png")
selenium.click("link=External Transfers Over a Threshold Dollar Amount")
selenium.waitForPageToLoad("30000")
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (External Transfers Over a Threshold Dollar Amount) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (External Transfers Over a Threshold Dollar Amount) after upgrade.png")
//selenium.click("link=Internal and External Transfer Velocity")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Internal and External Transfer Velocity) after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (Internal and External Transfer Velocity) after upgrade.png")
selenium.click("link=Invalid Login Attempts")
selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2&nbsp;&nbsp;- Orpheus reports (Invalid Login Attempts) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (Invalid Login Attempts) after upgrade.png")
selenium.click("link=Limits and Attempted Transfers")
selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Limits and Attempted Transfers) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (Limits and Attempted Transfers) after upgrade.png")
//selenium.click("link=Login Averages")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports ( Login Averages) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports ( Login Averages) after upgrade.png")
//Thread.sleep(3000)
//selenium.click("link=Pending External Accounts")
//selenium.waitForPageToLoad("30000")
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=btnExport")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Pending External Accounts) after upgrade.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Pending External Accounts) after upgrade.png")
selenium.click("link=Transfer Velocity by Channel")
selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (Transfer Velocity by Channel) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (Transfer Velocity by Channel) after upgrade.png")
selenium.click("link=User Accounts Enrolled in Online Banking")
selenium.waitForPageToLoad("30000")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("id=btnExportCsv")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Orpheus reports (User Accounts Enrolled in Online Banking) after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Orpheus reports (User Accounts Enrolled in Online Banking) after upgrade.png")
selenium.click("link=Logout")
