import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.junit.After
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/admin")
selenium.type("id=username", GlobalVariable.adminUserName)
selenium.type("id=password", GlobalVariable.adminPassword)
selenium.click("//input[@value='Sign in']")
selenium.waitForPageToLoad("30000")
selenium.click("link=ACH Configuration")
selenium.waitForPageToLoad("30000")
selenium.waitForPageToLoad("30000")
selenium.waitForPageToLoad("30000")
selenium.waitForPageToLoad("30000")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//span[text()='General Settings']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//span[text()='General Settings']")
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - ACH_GeneralSettings after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot1 + " - ACH_GeneralSettings.png")

selenium.click("//span[text()='NACHA Files']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[text()='NACHA Files Configuration']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - NACHA Files.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot1 + " - NACHA Files Configuration.png")

selenium.click("//span[text()='ACH Payments settings']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[text()='General settings for ACH Payments (Consumer & Business)']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - NACHA Files.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot1 + " - ACH Payments settings.png")

selenium.click("//span[text()='ACH Account Verification']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[text()='ACH Account Verification']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - NACHA Files.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot1 + " - ACH Account Verification.png")

selenium.click("link=Logout")
selenium.waitForPageToLoad("30000")

