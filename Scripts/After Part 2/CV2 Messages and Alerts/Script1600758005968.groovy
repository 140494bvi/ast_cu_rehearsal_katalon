import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/admin")
selenium.type("id=username", GlobalVariable.adminUserName)
selenium.type("id=password", GlobalVariable.adminPassword)
selenium.click("//input[@value='Sign in']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Messages and Alerts")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(1000);
selenium.click("link=Messages and Alerts")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//span[contains(text(),'Alerts administration')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//span[contains(text(),'Alerts administration')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[contains(text(), 'Share or Loan-specific Alerts')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(3000);
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 Alerts administration after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Alerts administration after.png")
selenium.click("//span[text()='Message templates']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[contains(text(), 'Recipient')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 Message templates after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Message templates after.png")
selenium.click("//span[contains(text(),'Message settings')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[contains(text(), 'Secure message maximum size:')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 Message settings after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Message settings after.png")
selenium.click("//span[contains(text(),'Alert channels')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[contains(text(), 'Default alert channels for custom alert')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 Alert channels after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Alert channels after.png")
selenium.click("//span[contains(text(),'Message auto reply')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[contains(text(), 'Secure message auto-reply feature')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 M&A Message auto reply after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Message auto reply after.png")
selenium.click("//span[contains(text(),'Admin Message templates')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[contains(text(), 'Admin Secure Messages Templates')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 Admin Message templates after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Admin Message templates after.png")
selenium.click("//span[contains(text(),'Alerts settings')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//h2[contains(text(), 'Configure the balance for balance alerts')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV-2 Alerts settings after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " M&A Alerts settings after.png")
selenium.click("link=Logout")
selenium.waitForPageToLoad("30000")
