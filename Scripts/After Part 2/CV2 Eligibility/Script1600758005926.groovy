import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/admin")
selenium.type("id=username", GlobalVariable.adminUserName)
selenium.type("id=password", GlobalVariable.adminPassword)
selenium.click("//input[@value='Sign in']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Eligibility")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(1000);
selenium.click("link=Eligibility")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//button[@id='switch-views']")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(3000);

//selenium.click("//span[contains(text(), '(XBEA:S0001)')]")
Thread.sleep(3000);
//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Eligibility S0004 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Eligibility.png")

//
//
//selenium.click("//div[@id='left-container']/div[11]/label/span")
//Thread.sleep(3000);
////selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Eligibility Business Receipts S0100 after.png", "")
//WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Eligibility Business Receipts S0100 after.png")
selenium.click("link=Logout")
selenium.waitForPageToLoad("30000")
