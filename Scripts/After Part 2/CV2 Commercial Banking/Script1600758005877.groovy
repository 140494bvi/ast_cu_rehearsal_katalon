import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium

import internal.GlobalVariable

import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/Login")
selenium.type("id=UserName", GlobalVariable.endUserName)
selenium.click("name=submit")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Return to login")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.type("id=Password", GlobalVariable.endPassword)
selenium.click("name=submit")
selenium.waitForPageToLoad("30000")
//
//for (int second = 0;; second++) {
//	if (second >= 60) fail("timeout");
//	try { if (selenium.isVisible("id=ChallengeQuestion")) break; } catch (Exception e) {}
//	Thread.sleep(1000);
//}
//selenium.type("id=ChallengeQuestion", "test")
//selenium.click("//button[@data-stable-name='ContinueButton']")
//selenium.waitForPageToLoad("30000")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Commercial Banking")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("link=Commercial Banking")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='groupListView']/div/div")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

Thread.sleep(3000)

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Banking Payroll groop after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Commercial Banking Payroll groop after.png")
Thread.sleep(2000)
selenium.click("//div[@id='wrapper__workarea']/section/div[2]/div/div/div[3]/ul/li[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='groupListView']/div/div")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

Thread.sleep(3000)

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Banking Receipts groop after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Commercial Banking Receipts groop after.png")
selenium.click("//div[@id='wrapper__workarea']/section/div[2]/div/div/div[3]/ul/li[3]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='groupListView']/div/div")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

Thread.sleep(3000)

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Banking payments groop after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Commercial Banking payments groop after.png")
selenium.click("//li[contains(text(), 'Payroll')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[5]/button[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[5]/button[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payroll Recipient 1')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payroll Recipient 1')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payroll Recipient 1 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payroll Recipient 1 after.png")
selenium.click("//li[contains(text(), 'Payroll')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[5]/button[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[5]/button[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payroll Recipient 2')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payroll Recipient 2')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payroll Recipient 2 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payroll Recipient 2 after.png")
selenium.click("//li[contains(text(), 'Receipts')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[5]/button[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[5]/button[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Receips recepient 1')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Receips recepient 1')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Receipts Recipients 1 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Receipts Recipients 1 after.png")
selenium.click("//li[contains(text(), 'Receipts')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[5]/button[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[5]/button[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Receips recepient 2')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Receips recepient 2')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Receipts Recipients 2 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Receipts Recipients 2 after.png")
selenium.click("//li[contains(text(), 'Payments')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[5]/button[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[5]/button[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payments recepient 1')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payments recepient 1')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payments Recipient 1 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payments Recipient 1 after.png")
selenium.click("//li[contains(text(), 'Payments')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[5]/button[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[5]/button[2]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payments recepient 2')]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

selenium.click("//div[@id='recipientsGrid']/div[2]/table/tbody/tr/td/span[contains(text(), 'Payments recepient 2')]")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@type='button'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payments Recipient 2 after.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Commercial Management Payments Recipient 2 after.png")
selenium.click("link=Sign out")
selenium.waitForPageToLoad("30000")
