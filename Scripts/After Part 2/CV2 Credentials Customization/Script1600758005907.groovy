import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import com.thoughtworks.selenium.Selenium
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.WebDriver
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium
import static org.junit.Assert.*
import java.util.regex.Pattern
import static org.apache.commons.lang3.StringUtils.join

WebUI.openBrowser('')
def driver = DriverFactory.getWebDriver()
String baseUrl = "https://www.katalon.com/"
selenium = new WebDriverBackedSelenium(driver, baseUrl)
WebUI.maximizeWindow()
selenium.open(GlobalVariable.baseURL + "/admin")
selenium.type("id=username", GlobalVariable.adminUserName)
selenium.type("id=password", GlobalVariable.adminPassword)
selenium.click("//input[@value='Sign in']")
for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("link=Credentials Customization")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}
Thread.sleep(1000);
selenium.click("link=Credentials Customization")
Thread.sleep(2000);
selenium.click("//span[text()='Username Requirements']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@name='Save'])[1]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Credentials customisation username requirements after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Credentials customisation username requirements after upgrade.png")
selenium.click("//span[text()='Password Requirements']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@name='Save'])[2]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Credentials customisation password requirements after upgrade.jpg", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Credentials customisation password requirements after upgrade.jpg")
selenium.click("//span[text()='Admin Username Requirements']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@name='Save'])[3]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Credentials customisation admin username req after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Credentials customisation admin username req after upgrade.png")
selenium.click("//span[text()='Admin Password Requirements']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@name='Save'])[4]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Credentials customisation admin password requirements after update.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Credentials customisation admin password requirements after update.png")
selenium.click("//span[text()='Security Image Settings']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@name='Save'])[5]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Credentials customisation security image after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Credentials customisation security image after upgrade.png")
selenium.click("//span[text()='Challenge Questions']")

for (int second = 0;; second++) {
	if (second >= 60) fail("timeout");
	try { if (selenium.isVisible("xpath=(//button[@name='Save'])[6]")) break; } catch (Exception e) {}
	Thread.sleep(1000);
}

//selenium.captureEntirePageScreenshot("C:\\AST\\CV\\Screenshots\\CV2 - Credentials customisation challenge questions after upgrade.png", "")
WebUI.takeScreenshot("C:\\AST\\CV\\Screenshots\\" + GlobalVariable.snapshot + " - Credentials customisation challenge questions after upgrade.png")
selenium.click("link=Logout")
selenium.waitForPageToLoad("30000")
