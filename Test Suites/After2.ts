<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>After2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5c20dabf-85d7-4bbb-9d00-cc2ff6ae9d3d</testSuiteGuid>
   <testCaseLink>
      <guid>5ee16e28-dbaf-40e6-b318-eeb0e83559cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Accounts and Transactions Customization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>702ea307-638e-49fd-a18b-db3416329e96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Branding</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>452610b6-2fc1-4551-b267-bfced0ed547a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Commercial Banking</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a68082f-5c85-443a-88ce-142156c1bd93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Configuration Overview</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3aeae937-63ad-488d-9599-4db1a9df74e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Credentials Customization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b69d0a8-ed91-4fb5-93c3-9312dbe3db0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Data Source Configuration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33d4812a-f78e-4d40-bf3a-a4412420a8ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Eligibility</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc7c0f81-70a2-4392-8c88-f028d272ec76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Enrollment Customization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>112e7459-6d23-4110-9089-ee805429b86d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Feature matrix</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dbfd973-6631-43f0-a028-36cd7695b64f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Limits</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2a61e38-767c-4d61-9a93-7f66062a5723</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Messages and Alerts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38e7171b-4fd4-4391-a2bc-16206b46749b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Repgen Call</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c509c5d-cda0-4b38-91f1-284d59a8dc69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Repgen Configuration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f446912-acb8-4db1-a24c-698364a3b891</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Resource Manager</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50921176-a1aa-45bb-af06-5923561f9ca4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Site Settings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25ba57b9-9633-452f-b5e6-0193ed5f7b87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 SMS Reporting</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d73cb240-b31c-44e8-b192-ca614d67e222</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Snapshots</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb9267df-4584-4fa6-8531-8d3d4ab16186</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 SSO Parameters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>245d51b7-0abf-4715-af5f-5f9cd5cb1c0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 T and P</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>931e065d-444a-4338-b0a0-5a559bd1e2af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Triggered Events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d789526-462a-491a-a66a-46f35517398f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 User Event Logs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47fd79a1-5baa-4b86-8d47-4e88d17e1c9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/CV2 Wizards Configuration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b412f62b-dc7d-4e52-9f20-9afeda212ae6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 2/SSO Parameters Edit Page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
