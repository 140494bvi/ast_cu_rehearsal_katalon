<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>EndUser</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4951f504-5e0f-4aa7-bca0-26275de2df6e</testSuiteGuid>
   <testCaseLink>
      <guid>871d5f60-05a6-4565-a28d-51f86e3aa26b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End User/Accounts End User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44fd69bc-f3a6-4f36-a322-b83caec6787f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End User/Bill Pay End User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1677ead9-ee34-4384-87b6-4325b98b1c84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End User/Family and Friends End User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99c7abaf-e389-4504-b0d1-2acf015d6d61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End User/Messages and Alerts End User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5721f66b-e9f4-4d96-8c51-ac0d96fabfd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End User/Settings End User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>487d8816-bdd5-49e8-8a50-00d0d6d34b53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End User/Transfer and Payments End User</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
