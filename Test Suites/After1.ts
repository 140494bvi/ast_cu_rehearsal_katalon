<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>After1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>41730671-4260-4d58-9c30-10b634790b0e</testSuiteGuid>
   <testCaseLink>
      <guid>e3e55882-1dbf-4e94-8387-a2298626ee89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 ACH Configuration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05cb80e5-8041-4f5f-92ee-5c5bc12a35fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Admin Audit Trail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>812bf964-89be-4620-bf54-b94aff3b1178</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Admin Management</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f3fd3db-bae3-41a4-b01c-e4ca48526ecf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Admin Role Management</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88152c77-5c3b-49eb-bcc6-1e9520400e3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Contacts and Settings</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>404cbfe9-1386-4717-807f-ea69433e3eb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 External Scheduled External Transfers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>718202cf-853f-4b54-a5d2-bdf11e1458c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Family and Friends End User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0308e11d-1070-487b-976c-6092aa5a23c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Family and Friends</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49dbda65-65a6-48fb-89a9-d98dd6449152</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Group Management</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d456b944-dc9e-428e-829b-842a83cb86e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Lockout Administration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>454990f1-5778-4c2f-b2bc-1be038a488e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Login and Retrieval</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc53aa88-6ad2-48a7-83e3-585cf6f7d1e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Navigation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d3fd46f-57f3-4e05-82c9-dfc1751f67fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Orpheus Reports</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4e0cb2a-3ecc-4e9c-a9a0-55d63693162b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Required System Emails</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff6b0d36-fb3c-4048-b112-b7570c42b4fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Support Sign On</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>846b6c5d-8fcb-4f2e-a59d-43594515f78f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 System Repgens</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7458b939-4c54-49ac-bb45-4af740b76c99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 Text Customization</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bdbfc64-8f1a-4bb9-96ce-aa7d3fb30ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/After Part 1/CV2 User Management</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
